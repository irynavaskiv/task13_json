package comparator;

import java.util.Comparator;

public class FlowerComparator implements Comparator<Flower> {
    public int compare(Flower flower1, Flower flower2) {
        if (flower1.getFl() > flower2.getFl()) {
            return 1;
        } else if (flower1.getFl() < flower2.getFl()) {
            return -1;
        } else {
            return 0;
        }
    }
}