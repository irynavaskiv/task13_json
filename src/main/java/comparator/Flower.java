package comparator;

public class Flower {

        private int fl;
        private String name;
        private String solid;
        private String origin;
        private String colorofthestem;
        private String thecoloroftheleaves;
        private int averageplantsize;
        private int temperature;
        private String loghting;
        private int watering;
        private String multiplying;

        public Flower() {}
        public int getFl() {
            return fl;
        }
        public String getName() {
            return name;
        }
        public String getSolid() {
            return solid;
        }
        public String getOrigin() {
            return origin;
        }
        public String getColorofthestem() {
            return colorofthestem;
        }
        public String getThecoloroftheleaves() {
            return thecoloroftheleaves;
        }
        public int getAverageplantsize() {
            return averageplantsize;
        }
        public int getTemperature() {
            return temperature;
        }
        public String getLoghting() {
            return loghting;
        }
        public int getWatering() {
            return watering;
        }
        public String getMultiplying() {
            return multiplying;
        }
        public Flower(int fl, String name, String solid, String origin, String colorofthestemstem, String thecoloroftheleaves, int averageplantsize, int temperature, String loghting, int watering, String multiplying) {

            this.fl = fl;
            this.name = name;
            this.solid = solid;
            this.origin = origin;
            this.colorofthestem = colorofthestem;
            this.averageplantsize = averageplantsize;
            this.temperature = temperature;
            this.loghting =loghting;
            this.watering =watering;
            this.multiplying =multiplying;
        }
    }


